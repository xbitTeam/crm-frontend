﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QWeb.Environments
{
    /// <summary>
    /// Develop environment.
    /// </summary>
    public class Develop
    {
        #region Properties

        /* 
         * App settings 
         */

        public static string SKIN_ID
        {
            get { return "100";  }
        }

        public static string SKIN_NAME
        {
            get { return "framework"; }
        }

        public static string DEFAULT_LANG
        {
            get { return "it"; }
        }

        /* 
         * Front-end settings 
         */

        public static string HOMEURL
        {
            get { return "http://localhost/metronicasptemplate"; }
        }

        public static string HOMEBASE
        {
            get { return "/metronicasptemplate/"; }
        }

        public static string[] WHITELIST
        {
            get { return new string[] { "self" }; }
        }

        /* 
         * API settings
         */

        public static string APIURL
        {
            get { return "http://yourapi.com"; }
        }

        public static string IMGURL
        {
            get { return "http://yourcdn.com"; }
        }

        /* 
         * Cookies settings 
         */

        public static string COOKIE_LANG
        {
            get { return "qweb_lang_develop"; }
        }

        #endregion
        
        #region Constructor

        public Develop()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #endregion
    }
}
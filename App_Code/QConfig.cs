﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

using QWeb.Environments;
using System.Reflection;

namespace QWeb {

    public class QConfig
    {
        #region Properties

        /*
         * General settings 
         */

        public static bool DEBUG
        {
            get 
            {
                bool debug = true;
                bool.TryParse( ConfigurationManager.AppSettings.Get("DEBUG"), out debug);
                return debug;
            }
        }

        public static string VERSION
        {
            get { return ConfigurationManager.AppSettings.Get("VERSION"); }
        }

        public static string ENVIRONMENT
        {
            get { return ConfigurationManager.AppSettings.Get("ENVIRONMENT"); }
        }

        /* 
         * App settings 
         */

        public static string SKIN_ID
        {
            get;
            set;
        }

        public static string SKIN_NAME
        {
            get;
            set;
        }

        public static string DEFAULT_LANG
        {
            get;
            set;
        }

        /* 
         * Front-end settings 
         */

        public static string HOMEURL
        {
            get;
            set;
        }

        public static string HOMEBASE
        {
            get;
            set;
        }

        public static string[] WHITELIST
        {
            get;
            set;
        }

        /* 
         * API settings
         */

        public static string APIURL
        {
            get;
            set;
        }

        public static string IMGURL
        {
            get;
            set;
        }

        /* 
         * Cookies settings 
         */

        public static string COOKIE_LANG
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        static QConfig()
        {
            Type qConfig = typeof(QConfig);
            Type qEnvironment = System.Reflection.Assembly.GetExecutingAssembly().GetType(string.Format("QWeb.Environments.{0}", ENVIRONMENT));

            if (qEnvironment == null)
                throw new Exception("Environment not found.");

            PropertyInfo[] qCnfProperties = qConfig.GetProperties(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
            PropertyInfo[] qEnvProperties = qEnvironment.GetProperties(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);

            foreach (PropertyInfo source in qEnvProperties)
            {
                PropertyInfo target = qCnfProperties.First<PropertyInfo>((PropertyInfo p) => { return p.Name.Equals(source.Name); });

                if (target != null)
                    target.SetValue(null, source.GetValue(null));
            }
        }

        public QConfig()
        {

        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

using QWeb;

public partial class Config_js : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScriptSerializer json = new JavaScriptSerializer();

        QPublicConfig config = new QPublicConfig();
        config.SKIN_ID = QConfig.SKIN_ID;
        config.SKIN_NAME = QConfig.SKIN_NAME;
        config.DEFAULT_LANG = QConfig.DEFAULT_LANG;
        config.DEBUG = QConfig.DEBUG;
        config.HOMEURL = QConfig.HOMEURL;
        config.HOMEBASE = QConfig.HOMEBASE;
        config.WHITELIST = QConfig.WHITELIST;
        config.APIURL = QConfig.APIURL;
        config.IMGURL = QConfig.IMGURL;
        config.COOKIE_LANG = QConfig.COOKIE_LANG;

        Response.Clear();
        Response.ContentType = "application/json; charset=utf-8";
        Response.Write(json.Serialize(config));
        Response.End();
    }

    #region Internal class

    public class QPublicConfig
    {
        /* 
         * App settings 
         */

        public string SKIN_ID
        {
            get;
            set;
        }

        public string SKIN_NAME
        {
            get;
            set;
        }

        public string DEFAULT_LANG
        {
            get;
            set;
        }
        public bool DEBUG
        {
            get;
            set;
        }


        /* 
         * Front-end settings 
         */

        public string HOMEURL
        {
            get;
            set;
        }

        public string HOMEBASE
        {
            get;
            set;
        }

        public string[] WHITELIST
        {
            get;
            set;
        }

        /* 
         * API settings
         */

        public  string APIURL
        {
            get;
            set;
        }

        public string IMGURL
        {
            get;
            set;
        }

        /* 
         * Cookies settings 
         */

        public string COOKIE_LANG
        {
            get;
            set;
        }
    }

    #endregion
}
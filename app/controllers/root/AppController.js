/* Setup App Main Controller */
mainApp.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function() {
        Metronic.setAssetsPath( $scope.settings.HOMEBASE +  'assets/'); // Set the assets folder path   
        Metronic.initComponents(); // init core components
        Metronic.init(); // Run metronic theme
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 
    });
}]);
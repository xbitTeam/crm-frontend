/**
 * Metronic AngularJS App Main Script
 */

/* Metronic App */
var mainApp = angular
.module("mainApp", [
    "ui.router", 
    "ui.bootstrap", 
    "oc.lazyLoad",  
    "ngSanitize",
    "datatables"
])
.config(['$ocLazyLoadProvider', '$stateProvider', '$urlRouterProvider', '$sceDelegateProvider', '$locationProvider', '$httpProvider', 
function( $ocLazyLoadProvider,   $stateProvider,   $urlRouterProvider,   $sceDelegateProvider,   $locationProvider,   $httpProvider) {

    $ocLazyLoadProvider.config({
        cssFilesInsertBefore: 'ng_load_plugins_before' // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
    });

    $httpProvider.interceptors.push('fTokenInterceptor');

    window.QConfig = JSON.parse($.ajax({
        type : "GET",
        async : false,
        crossDomain : false,
        url : 'Config.js.aspx',
        processData : false
    }).responseText);

    if (window.QConfig && window.QConfig.DEBUG && window.console) {
        console.log(window.QConfig);
    };

    // ResourceUrlWhitelist
    $sceDelegateProvider.resourceUrlWhitelist(window.QConfig.WHITELIST);

    // ISO 639-1 (two character)
    var defaultLang = $.cookie(window.QConfig.COOKIE_LANG);
    if ((defaultLang === null) || (defaultLang === '') || (typeof defaultLang === 'undefined') || defaultLang.length !== 2) {
        defaultLang = window.QConfig.DEFAULT_LANG;
    }
    
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/login");
    
    $stateProvider
        
        .state('login',{
            url : "/login",
            templateUrl : "app/views/login/login.html",
            data : { pageTitle : "Login Page" },
            controller : "Login",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'mainApp',
                        files: [
                            window.QConfig.HOMEBASE + 'assets/admin/pages/css/login4.css',
                            window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-toastr/toastr.min.css',
                            window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-toastr/toastr.min.js',
                            window.QConfig.HOMEBASE + 'app/controllers/login/Login.js'
                        ] 
                    });
                }]
            }
        })

        // Dashboard
        .state('dashboard', {
            url: "/dashboard",
            templateUrl: "app/views/dashboard.html",            
            data: {pageTitle: 'Admin Dashboard Template'},
            controller: "DashboardController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'mainApp',
                        files: [
                           window.QConfig.HOMEBASE + 'assets/global/plugins/morris/morris.css',
                           window.QConfig.HOMEBASE + 'assets/admin/pages/css/tasks.css',
                            
                           window.QConfig.HOMEBASE + 'assets/global/plugins/morris/morris.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/morris/raphael-min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/jquery.sparkline.min.js',

                           window.QConfig.HOMEBASE + 'assets/admin/pages/scripts/index3.js',
                           window.QConfig.HOMEBASE + 'assets/admin/pages/scripts/tasks.js',

                           window.QConfig.HOMEBASE + 'app/controllers/DashboardController.js'
                        ] 
                    });
                }]
            }
        })

        .state('simpleTable', {
            url: "/datatables/simple-table",
            templateUrl: "app/views/datatables/simple-table.html",            
            data: {pageTitle: 'Simple Table'},
            controller: "SimpleTable",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'mainApp',
                        files: [
                    
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.css',                             
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datepicker/css/datepicker.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-toastr/toastr.min.css',

                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/datatables/all.min.js',
                           window.QConfig.HOMEBASE + 'assets/admin/pages/scripts/simple-table.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-toastr/toastr.min.js',

                           window.QConfig.HOMEBASE + 'app/controllers/SimpleTable.js'
                        ] 
                    });
                }]
            }
        })
        

        // AngularJS plugins
        .state('fileupload', {
            url: "/file_upload",
            templateUrl: "app/views/file_upload.html",
            data: {pageTitle: 'AngularJS File Upload'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'angularFileUpload',
                        files: [
                           window.QConfig.HOMEBASE + 'assets/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                        ] 
                    }, {
                        name: 'mainApp',
                        files: [
                            window.QConfig.HOMEBASE + 'app/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // UI Select
        .state('uiselect', {
            url: "/ui_select",
            templateUrl: "app/views/ui_select.html",
            data: {pageTitle: 'AngularJS Ui Select'},
            controller: "UISelectController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'ui.select',
                        files: [
                           window.QConfig.HOMEBASE + 'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ] 
                    }, {
                        name: 'mainApp',
                        files: [
                            window.QConfig.HOMEBASE + 'app/controllers/UISelectController.js'
                        ] 
                    }]);
                }]
            }
        })

        // UI Bootstrap
        .state('uibootstrap', {
            url: "/ui_bootstrap",
            templateUrl: "app/views/ui_bootstrap.html",
            data: {pageTitle: 'AngularJS UI Bootstrap'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'mainApp',
                        files: [
                            window.QConfig.HOMEBASE + 'app/controllers/GeneralPageController.js'
                        ] 
                    }]);
                }] 
            }
        })

        // Tree View
        .state('tree', {
            url: "/tree",
            templateUrl: "app/views/tree.html",
            data: {pageTitle: 'jQuery Tree View'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'mainApp',
                        files: [
                           window.QConfig.HOMEBASE + 'assets/global/plugins/jstree/dist/themes/default/style.min.css',

                           window.QConfig.HOMEBASE + 'assets/global/plugins/jstree/dist/jstree.min.js',
                           window.QConfig.HOMEBASE + 'assets/admin/pages/scripts/ui-tree.js',
                           window.QConfig.HOMEBASE + 'app/controllers/GeneralPageController.js'
                        ] 
                    }]);
                }] 
            }
        })     

        // Form Tools
        .state('formtools', {
            url: "/form-tools",
            templateUrl: "app/views/form_tools.html",
            data: {pageTitle: 'Form Tools'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'mainApp',
                        files: [
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/jquery-tags-input/jquery.tagsinput.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/typeahead/typeahead.css',

                           window.QConfig.HOMEBASE + 'assets/global/plugins/fuelux/js/spinner.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/jquery.input-ip-address-control-1.0.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/typeahead/handlebars.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/typeahead/typeahead.bundle.min.js',
                           window.QConfig.HOMEBASE + 'assets/admin/pages/scripts/components-form-tools.js',

                           window.QConfig.HOMEBASE + 'app/controllers/GeneralPageController.js'
                        ] 
                    }]);
                }] 
            }
        })        

        // Date & Time Pickers
        .state('pickers', {
            url: "/pickers",
            templateUrl: "app/views/pickers.html",
            data: {pageTitle: 'Date & Time Pickers'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'mainApp',
                        files: [
                           window.QConfig.HOMEBASE + 'assets/global/plugins/clockface/css/clockface.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datepicker/css/datepicker3.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/clockface/js/clockface.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-daterangepicker/moment.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                           window.QConfig.HOMEBASE + 'assets/admin/pages/scripts/components-pickers.js',

                           window.QConfig.HOMEBASE + 'app/controllers/GeneralPageController.js'
                        ] 
                    }]);
                }] 
            }
        })

        // Custom Dropdowns
        .state('dropdowns', {
            url: "/dropdowns",
            templateUrl: "app/views/dropdowns.html",
            data: {pageTitle: 'Custom Dropdowns'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'mainApp',
                        files: [
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-select/bootstrap-select.min.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/jquery-multi-select/css/multi-select.css',

                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-select/bootstrap-select.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js',

                           window.QConfig.HOMEBASE + 'assets/admin/pages/scripts/components-dropdowns.js',

                           window.QConfig.HOMEBASE + 'app/controllers/GeneralPageController.js'
                        ] 
                    }]);
                }] 
            }
        }) 

        // Advanced Datatables
        .state('datatablesAdvanced', {
            url: "/datatables/advanced.html",
            templateUrl: "app/views/datatables/advanced.html",
            data: {pageTitle: 'Advanced Datatables'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'mainApp',
                        files: [
                           window.QConfig.HOMEBASE + 'assets/admin/pages/css/datatable.css',                             
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.css',                             
                           window.QConfig.HOMEBASE + 'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css', 
                           window.QConfig.HOMEBASE + 'assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css',

                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/datatables/all.min.js',
                           window.QConfig.HOMEBASE + 'app/scripts/table-advanced.js',

                           window.QConfig.HOMEBASE + 'app/controllers/GeneralPageController.js'
                        ]
                    });
                }]
            }
        })   


        .state("angularTable", {
            url : "/datatables/angular-table",
            templateUrl : "app/views/datatables/angular-table.html",
            data : { pageTitle : "Angular table" },
            controller : "AngularTable",
            resolve : {
                deps : ["$ocLazyLoad", function($ocLazyLoad){
                    return $ocLazyLoad.load({
                        name : "mainApp",
                        files : [

                           window.QConfig.HOMEBASE + 'assets/admin/pages/css/datatable.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-toastr/toastr.min.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.css',                             
                           window.QConfig.HOMEBASE + 'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',

                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-toastr/toastr.min.js',

                           window.QConfig.HOMEBASE + 'app/factories/Demo/fProfiles.js',

                           window.QConfig.HOMEBASE + 'app/controllers/AngularTable.js'


                        ]
                    })
                }]
            }
        })


        // Ajax Datetables
        .state('datatablesAjax', {
            url: "/datatables/ajax",
            templateUrl: "app/views/datatables/ajax.html",
            data: {pageTitle: 'Ajax Datatables'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'mainApp',
                        files: [
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.css',                             
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datepicker/css/datepicker.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',

                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/datatables/all.min.js',

                           window.QConfig.HOMEBASE + 'assets/global/scripts/datatable.js',
                           window.QConfig.HOMEBASE + 'app/scripts/table-ajax.js',

                           window.QConfig.HOMEBASE + 'app/controllers/GeneralPageController.js'
                        ]
                    });
                }]
            }
        })

        .state("angularAdvancedTable", {
            url : "/datatables/angular-advanced-table",
            templateUrl : "app/views/datatables/angular-advanced-table.html",
            data : { pageTitle : "Angular advanced table" },
            controller : "AngularAdvancedTable",
            resolve : {
                deps : ["$ocLazyLoad", function($ocLazyLoad){
                    return $ocLazyLoad.load({
                        name : "mainApp",
                        files : [

                           window.QConfig.HOMEBASE + 'assets/admin/pages/css/datatable.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-toastr/toastr.min.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.css',                             
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datepicker/css/datepicker.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css',

                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-toastr/toastr.min.js',

                           window.QConfig.HOMEBASE + 'app/filters/unique.js',
                           window.QConfig.HOMEBASE + 'app/directives/fix.js',
                           window.QConfig.HOMEBASE + 'app/factories/Demo/fProfiles.js',
                            
                           window.QConfig.HOMEBASE + 'app/controllers/AngularAdvancedTable.js'


                        ]
                    })
                }]
            }
        })



        // User Profile
        .state("profile", {
            url: "/profile",
            templateUrl: "app/views/profile/main.html",
            data: {pageTitle: 'User Profile'},
            controller: "UserProfileController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'mainApp',  
                        files: [
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                           window.QConfig.HOMEBASE + 'assets/admin/pages/css/profile.css',
                           window.QConfig.HOMEBASE + 'assets/admin/pages/css/tasks.css',
                            
                           window.QConfig.HOMEBASE + 'assets/global/plugins/jquery.sparkline.min.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',

                           window.QConfig.HOMEBASE + 'assets/admin/pages/scripts/profile.js',

                           window.QConfig.HOMEBASE + 'app/controllers/UserProfileController.js'
                        ]                    
                    });
                }]
            }
        })

        // User Profile Dashboard
        .state("profile.dashboard", {
            url: "/dashboard",
            templateUrl: "app/views/profile/dashboard.html",
            data: {pageTitle: 'User Profile'}
        })

        // User Profile Account
        .state("profile.account", {
            url: "/account",
            templateUrl: "app/views/profile/account.html",
            data: {pageTitle: 'User Account'}
        })

        // User Profile Help
        .state("profile.help", {
            url: "/help",
            templateUrl: "app/views/profile/help.html",
            data: {pageTitle: 'User Help'}      
        })

        // Todo
        .state('todo', {
            url: "/todo",
            templateUrl: "app/views/todo.html",
            data: {pageTitle: 'Todo'},
            controller: "TodoController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({ 
                        name: 'mainApp',  
                        files: [
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datepicker/css/datepicker3.css',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.css',
                           window.QConfig.HOMEBASE + 'assets/admin/pages/css/todo.css',
                            
                           window.QConfig.HOMEBASE + 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
                           window.QConfig.HOMEBASE + 'assets/global/plugins/select2/select2.min.js',

                           window.QConfig.HOMEBASE + 'assets/admin/pages/scripts/todo.js',

                           window.QConfig.HOMEBASE + 'app/controllers/TodoController.js'  
                        ]                    
                    });
                }]
            }
        });
		
	// To remove the url hashtag (#) set html5 mode (http://docs.angularjs.org/guide/dev_guide.services.$location)
	$locationProvider.html5Mode(true);
}])
.run(  [ "$rootScope", "$state", "fLogin", 
function( $rootScope,   $state,   fLogin) {

    $rootScope.$state = $state; // state to be accessed from view
    console.log($state)
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        }
        //,layoutImgPath: Metronic.getAssetsPath() + 'admin/layout/img/'
        //,layoutCssPath: Metronic.getAssetsPath() + 'admin/layout/css/'
    };
	
	for (var i in window.QConfig) {
		settings[i] = window.QConfig[i];
	}

    $rootScope.settings = settings;
 
    /**
     * array of routes that doesn't need authentication
     * @type {Array}
     */
    var routesThatDontRequireAuth = ['/login'];

    /**
     * check if the current route don't need authentication
     * @param  {string} route
     * @return {string}
     */
    var routeClean = function (route) {
        return _.find(routesThatDontRequireAuth, function (noAuthRoute) {
            return _.str.startsWith(route, noAuthRoute);
        });
    };

    $rootScope.$on("$stateChangeStart", function(event, toState, toStateParams){
        /**
         * if you are not logged in the user will be redirected
         * to the login page
         */
        if (!routeClean(toState.url) && !fLogin.isLoggedIn(true)) {
            event.preventDefault();
            $state.go('login');
        }   
    });

}]);
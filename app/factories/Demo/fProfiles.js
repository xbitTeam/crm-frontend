mainApp.factory('fProfiles', ['$http', function($http){
	var _apiURL = "assets/admin/pages/demo/profiles.json";
	return {
		profiles : function(){
			return $http({
				url : _apiURL,
				cache : true,
				method : "GET"
			});
		}
	};
}])
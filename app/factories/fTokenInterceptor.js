mainApp.factory('fTokenInterceptor', ['$injector', function($injector){
	return {
		/**
		 * here we can watch if the token is saved in the http headers
		 * and the user can make http calls 
		 * @param  {object} config
		 * @return {object}
		 */
		request : function(config){
			/**
			 * the fLogin service must be injected via $injector
			 * otherwise throws an error
			 * @type {[type]}
			 */
			var fLogin = $injector.get('fLogin')
				_token = config.headers['auth-token'];

			if (_token) {
				console.log('a token exists in the http headers!');
				if (fLogin.tokenMatch(_token)) {
					console.log('the token is equal to the saved token!');
				}else{
					console.log('this is not our token!');
				}
			}

			return config;
		}
	};
}]);
mainApp.factory('fEffects', ['$rootScope', 
                         function($rootScope) {

	var $this = [];

	/**
	 * css effect.
	 * 
	 * @param selector
	 * @param opt
	 */
	$this.css = function(selector, opt) {
        var 
        DEFAULTS = {
		    /* Empty */
        }, 
        options = $.extend({}, DEFAULTS, opt || {});
		
		$(selector).css(options);
	};

    /**
     * Shake effect.
     * 
     * @param selector
     */
    $this.shake = function(selector){  
        $(selector).effect("shake", "slow");
    };

	
	return $this;
}]);